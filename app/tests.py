from django.test import TestCase
from django.urls import resolve
from .views import *
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

class UnitTest(TestCase):
    def testPageIsExist(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def testPageUsesIndexHTML(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'index.html')

    def testPageUsingIndexFunc(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def testPageHasTable(self):
        response = self.client.get('/')
        self.assertContains(response, '<table class="table')

    def testPageHasSearchBar(self):
        response = self.client.get('/')
        self.assertContains(response, 'type="search"')

class FunctionalTest(TestCase):
    browser = None
    
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.browser.get("http://127.0.0.1:8000")
        super(FunctionalTest,self).setUp() 

    def tearDown(self):
        self.browser.quit()
        super(FunctionalTest, self).tearDown()

    
    def testPageDisplaysRightBook(self):
        searchBar = self.browser.find_element_by_id("search")
        searchBar.send_keys("java")

        self.assertIn(self.browser.page_source, "Pemrograman Berbasis Objek Dengan Bahasa Java")
