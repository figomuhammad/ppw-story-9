$(document).ready(function() {
    $("#search").on('keyup', delay(function () {
        var url = "https://www.googleapis.com/books/v1/volumes?q=" + $(this).val();

        $.getJSON(url,
            function (data, textStatus, jqXHR) {
                var books = data["items"];
                result = makeHTMLJSON(books);
                $("tbody").html(result);
            }
        );
    }, 100));
});

function makeHTMLJSON(books) {
    var result = "";
    $.each(books, function (index, value) { 
        var sampul = "<td>" + '<img style="width: 50%;" src="' + value.volumeInfo.imageLinks.smallThumbnail + '"></img>' + "</td>";
        var judul = '<td>' + value.volumeInfo.title + "</td>";

        var penulis = "<td>";
        $.each(value.volumeInfo.authors, function (index, value) { 
            penulis += value + "<br>";
        });
        penulis += "</td>";

        var penerbit = "<td>" + value.volumeInfo.publisher + "</td>";
        var tahunTerbit = "<td>" + value.volumeInfo.publishedDate + "</td>";

        var kategori = "<td>";
        $.each(value.volumeInfo.categories, function (index, value) { 
            kategori += value + "<br>";
        });
        kategori += "</td>";
        
        var likeColumn = '<td class="like"><p id="count' + index + '">0</p><button type="button" onclick="likeButton(' + index + ')" class="btn btn-dark mb-1">Like</button></td>';
        result += '<tr>' + sampul + judul + penulis + penerbit + tahunTerbit + kategori + likeColumn + "</tr>";
    });
    return result;
}

// https://stackoverflow.com/questions/1909441/how-to-delay-the-keyup-handler-until-the-user-stops-typing
function delay(callback, ms) {
    var timer = 0;
    return function() {
        var context = this, args = arguments;
        clearTimeout(timer);
        timer = setTimeout(function () {
            callback.apply(context, args);
        }, ms || 0);
    };
}

function likeButton(index) {
    incrementLike(index);
    rankLikes();
}

function incrementLike(index) {
    var id = "count" + index;
    var val = document.getElementById(id).innerHTML;
    val++;
    document.getElementById(id).innerHTML = val;
}

function rankLikes() {
    var likes = $(".like");
    for (var i=0; i<likes.length; i++) {
        for (var j=0; j<likes.length-i-1; j++) {
            if (likes[j].innerHTML > likes[j+1].innerHTML) {
                var temp = likes[j];
                likes[j] = likes[j+1];
                likes[j+1] = temp;
            }
        }
    }

    var modal = $(".modal-body")
    modal.innerHTML = "";
    for (var i=0; i<5; i++) {
        modal.innerHTML += likes[i].childNodes[0].innerHTML + "<br>";
    }
    console.log(modal.innerHTML);
}
